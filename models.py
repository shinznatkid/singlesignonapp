# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django.utils.encoding import python_2_unicode_compatible
from django.contrib.auth.models import User
from django.db import models


def user_string(self):
    if self.first_name:
        return self.first_name
    return self.username

User.nickname = user_string


def user_str(self):
    return self.nickname()

User.__str__ = user_str
User.__unicode__ = user_str
